/*
 * This work is licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.apache.ode.bpel.common.evt;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ode.bpel.iapi.BpelEventListener;
import org.apache.ode.bpel.evt.ActivityExecStartEvent;
import org.apache.ode.bpel.evt.BpelEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Implementation of the {@link BpelEventListener} interface that exports
 * executed activities into a CSV file to enable process mining.
 * If no output folder is given, the listener exports to stdout.
 * 
 * To use the CSVExportBpelEventListener add the following lines to your 
 * ode-xxx.properties file:
 * <code>
 * ode-xxx.event.listeners=org.apache.ode.bpel.common.evt.CSVExportBpelEventListener
 * csvexportbpeleventlistener.outputFolder=$your_output_folder
 * </code>
 * 
 * @author Richard Müller (Eindhoven Technical University)
 */
public class CSVExport implements BpelEventListener {
	
	// the logging facilities of ODE
	private static final Log serverLog = LogFactory.getLog(BpelEventListener.class);
	// how to format dates
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
	// the keyword to look for in the ode-xxx.properties file
	private static final String OUTPUTDIRECTORY_KEY = "csvexportbpeleventlistener.outputDirectory";
	// the output directory; a value of null implies output to stdout
	private static String outputDirectory = null;
	// the output writer
	private static BufferedWriter output = null;
	
	/**
	 * Store case ID, activity, and timestamp to CSV file.
	 */
	public void onEvent(BpelEvent bpelEvent) {
		assert serverLog != null;
    	if (serverLog.isDebugEnabled()) {
    		serverLog.debug("BPELEventListener: "+ CSVExport.class +" received an event!");
		}
    	
    	assert bpelEvent != null;
    	if (bpelEvent instanceof ActivityExecStartEvent) {
    		final ActivityExecStartEvent startEvent = (ActivityExecStartEvent) bpelEvent;
    		final String line = startEvent.getProcessInstanceId() + ";" +
    				startEvent.getActivityName() + ";" +
    				sdf.format(startEvent.getTimestamp());
    		if (output != null) {
    			try {
    	        	output.write(line);
    	        	output.newLine();
    	        	output.flush();
    	        } catch (final Exception e) {
    	        	e.printStackTrace();
    	        }
        	} else {
        		System.out.println(line);
        	}
    	}
    	
    	// just for debugging
//        System.out.println("\nEvent Notification: " +  
//                bpelEvent.getType().toString() +  
//                ", " + bpelEvent.getTimestamp()+  
//                ", " + bpelEvent.toString());
    }
	
	/**
	 * Called at startup of the listener.
	 * Initializes the output.
	 */
    public void startup(Properties configProperties) {
    	assert serverLog != null;
    	if (serverLog.isDebugEnabled()) {
    		serverLog.debug("BPELEventListener: "+ CSVExport.class +" startup!");
		}
    	
    	// check whether we output to a file or to stdout
    	if (configProperties != null) {
			outputDirectory = configProperties.getProperty(OUTPUTDIRECTORY_KEY);
		}
    	
    	// prepare output to file
    	if (outputDirectory != null) {
    		final File file = new File(outputDirectory + sdf.format(new Date()) + ".csv");
    		
    		try {
    			if (file.getParent() != null) {
    				file.getParentFile().mkdirs();
    			}
    			file.createNewFile();
    			final FileWriter fstream = new FileWriter(file);
    			output = new BufferedWriter(fstream);
    		} catch (final Exception e) {
    			e.printStackTrace();
    		}
    		assert output != null;
    	}
    }
    
    /**
     * Called at shutdown of the listener.
     * Closes the output.
     */
    public void shutdown() {
    	assert serverLog != null;
    	if (serverLog.isDebugEnabled()) {
    		serverLog.debug("BPELEventListener: "+ CSVExport.class +" shutdown!");
		}
    	
    	// close output to file
    	if (output != null) {
    		try {
    			output.close();
    		} catch (final Exception e) {
    			e.printStackTrace();
    		}
    	}
    }
}
