# Content

This work contains an event listener for Apache ODE that exports executed activities into a CSV file to enable service mining.

# Literature
* van der Aalst, W.M.P.: Service Mining: Using Process Mining to Discover, Check, and Improve Service Behavior. IEEE Transactions on Services Computing. 1 (2012).
* Müller, R., Stahl, C., van der Aalst, W.M.P., Westergaard, M.: Service Discovery from Observed Behavior while Guaranteeing Deadlock Freedom in Collaborations. In: Basu, S., Pautasso, C., Zhang, L., and Fu, X. (eds.) Lecture Notes in Computer Science 8274. pp. 358–373–15. Springer Berlin Heidelberg (2013).

# Building

Include into Apache ODE 1.3.6 (or higher) and build ODE.

# Copyright

This work is licensed under the Apache License, Version 2.0
(the "License"); you may not use this work except in compliance with the License.  You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

